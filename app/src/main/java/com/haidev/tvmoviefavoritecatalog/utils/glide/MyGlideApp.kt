package com.haidev.tvmoviefavoritecatalog.utils.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


@GlideModule
class MyGlideApp : AppGlideModule()