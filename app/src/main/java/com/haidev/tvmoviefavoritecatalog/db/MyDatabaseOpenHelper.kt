package com.haidev.tvmoviefavoritecatalog.db

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavMovieModel
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavTvModel
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(
    ctx, "db_favorite",
    null, 1
) {

    companion object {

        const val AUTHORITY = "com.haidev.tvmoviefavoritecatalog"
        private const val SCHEME = "content"

        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MyDatabaseOpenHelper
        }

        val CONTENT_URI_MOVIE = Uri.Builder().scheme(SCHEME)
            .authority(AUTHORITY).appendPath(FavMovieModel.TABLE_FAVORITE_MOVIE).build()

    }

    fun queryProviderMovies(db: MyDatabaseOpenHelper): Cursor {
        return db.use {
            this.query(
                FavMovieModel.TABLE_FAVORITE_MOVIE,
                null, null, null,
                null, null, null
            )
        }
    }


    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
            FavMovieModel.TABLE_FAVORITE_MOVIE, true,
            FavMovieModel.ID_FAVORITE_MOVIE to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            FavMovieModel.TITLE_FAVORITE_MOVIE to TEXT,
            FavMovieModel.OVERVIEW_FAVORITE_MOVIE to TEXT,
            FavMovieModel.POSTER_FAVORITE_MOVIE to TEXT,
            FavMovieModel.DATE_FAVORITE_MOVIE to TEXT
        )

        db?.createTable(
            FavTvModel.TABLE_FAVORITE_TV, true,
            FavTvModel.ID_FAVORITE_TV to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            FavTvModel.TITLE_FAVORITE_TV to TEXT,
            FavTvModel.OVERVIEW_FAVORITE_TV to TEXT,
            FavTvModel.POSTER_FAVORITE_TV to TEXT,
            FavTvModel.DATE_FAVORITE_TV to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // menghapus table
        db?.dropTable(FavMovieModel.TABLE_FAVORITE_MOVIE, true)
        db?.dropTable(FavTvModel.TABLE_FAVORITE_TV, true)
    }
}

// extension function
val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)