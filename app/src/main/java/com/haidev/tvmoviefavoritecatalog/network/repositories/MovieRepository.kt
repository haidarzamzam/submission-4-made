package com.haidev.tvmoviefavoritecatalog.network.repositories

import com.haidev.tvmoviefavoritecatalog.menu.movie.models.MovieModel
import com.haidev.tvmoviefavoritecatalog.network.ApiObserver
import com.haidev.tvmoviefavoritecatalog.network.ServiceFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MovieRepository {
    private val compositeDisposable = CompositeDisposable()
    private val apiService = ServiceFactory.create()

    fun getListMovie(onResult: (MovieModel) -> Unit, onError: (Throwable) -> Unit) {
        apiService.getListMovie()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<MovieModel>(compositeDisposable) {
                override fun onApiSuccess(data: MovieModel) {
                    onResult(data)
                }

                override fun onApiError(er: Throwable) {
                    onError(er)
                }
            })
    }

    fun getSearchMovie(onResult: (MovieModel) -> Unit, onError: (Throwable) -> Unit, keyword: String) {
        apiService.getSearchMovie(keyword)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<MovieModel>(compositeDisposable) {
                override fun onApiSuccess(data: MovieModel) {
                    onResult(data)
                }

                override fun onApiError(er: Throwable) {
                    onError(er)
                }
            })
    }

    fun getReleaseMovie(
        onResult: (MovieModel) -> Unit,
        onError: (Throwable) -> Unit,
        primaryReleaseGte: String,
        primaryReleaseLte: String
    ) {
        apiService.getMovieRelease(primaryReleaseGte, primaryReleaseLte)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<MovieModel>(compositeDisposable) {
                override fun onApiSuccess(data: MovieModel) {
                    onResult(data)
                }

                override fun onApiError(er: Throwable) {
                    onError(er)
                }
            })
    }

    fun cleared() {
        compositeDisposable.clear()
    }


}