package com.haidev.tvmoviefavoritecatalog.network.repositories

import com.haidev.tvmoviefavoritecatalog.menu.tv.models.TvModel
import com.haidev.tvmoviefavoritecatalog.network.ApiObserver
import com.haidev.tvmoviefavoritecatalog.network.ServiceFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class TvRepository {
    private val compositeDisposable = CompositeDisposable()
    private val apiService = ServiceFactory.create()

    fun getListTv(onResult: (TvModel) -> Unit, onError: (Throwable) -> Unit) {
        apiService.getListTv()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<TvModel>(compositeDisposable) {
                override fun onApiSuccess(data: TvModel) {
                    onResult(data)
                }

                override fun onApiError(er: Throwable) {
                    onError(er)
                }
            })
    }

    fun getSearchTv(onResult: (TvModel) -> Unit, onError: (Throwable) -> Unit, keyword: String) {
        apiService.getSearchTv(keyword)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<TvModel>(compositeDisposable) {
                override fun onApiSuccess(data: TvModel) {
                    onResult(data)
                }

                override fun onApiError(er: Throwable) {
                    onError(er)
                }
            })
    }
    fun cleared() {
        compositeDisposable.clear()
    }
}