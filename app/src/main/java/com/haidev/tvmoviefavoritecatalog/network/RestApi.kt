package com.haidev.tvmoviefavoritecatalog.network

import com.haidev.tvmoviefavoritecatalog.menu.movie.models.MovieModel
import com.haidev.tvmoviefavoritecatalog.menu.tv.models.TvModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RestApi {

    @GET("discover/movie?api_key=3d31335928249bca6a9f04d1d9d85d03&language=en-US")
    fun getListMovie(): Observable<MovieModel>

    @GET("search/movie?api_key=3d31335928249bca6a9f04d1d9d85d03&language=en-US&}")
    fun getSearchMovie(
        @Query("query") keyword: String
    ): Observable<MovieModel>

    @GET("discover/tv?api_key=3d31335928249bca6a9f04d1d9d85d03&language=en-US")
    fun getListTv(): Observable<TvModel>

    @GET("search/tv?api_key=3d31335928249bca6a9f04d1d9d85d03&language=en-US&}")
    fun getSearchTv(
        @Query("query") keyword: String
    ): Observable<TvModel>

    @GET("discover/movie?api_key=3d31335928249bca6a9f04d1d9d85d03&")
    fun getMovieRelease(
        @Query("primary_release_date.gte") primaryReleaseGte: String,
        @Query("primary_release_date.lte") primaryReleaseLte: String
    ): Observable<MovieModel>
}