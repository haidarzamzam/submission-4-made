package com.haidev.tvmoviefavoritecatalog.alarm.service

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.main.views.MainActivity
import com.haidev.tvmoviefavoritecatalog.menu.movie.models.MovieModel
import com.haidev.tvmoviefavoritecatalog.network.ApiObserver
import com.haidev.tvmoviefavoritecatalog.network.ServiceFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

class ReleaseMovieReminder : BroadcastReceiver() {

    private var listMovie: MutableList<MovieModel.ResultsMovie> = mutableListOf()
    private val compositeDisposable = CompositeDisposable()
    private val apiService = ServiceFactory.create()

    val NOTIFICATION_ID = 2
    var CHANNEL_ID = "channel_02"
    var CHANNEL_NAME: CharSequence = "haidev channel"

    override fun onReceive(context: Context, intent: Intent) {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date = Date()
        val currentDate = sdf.format(date)
        //        final String currentDate = "2018-08-18";

        getReleaseMovie(currentDate, context)
        notifId = intent.getIntExtra("id", 0)
        val title = intent.getStringExtra("movieTitle")

    }

    fun getReleaseMovie(
        date: String,
        context: Context
    ) {
        apiService.getMovieRelease(date, date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<MovieModel>(compositeDisposable) {
                override fun onApiSuccess(data: MovieModel) {
                    listMovie.clear()
                    listMovie.addAll(data.resultsMovie)
                    Log.d("jumlahDataAPI", listMovie.size.toString())
                    if (listMovie.size == 0) {
                        showAlarmNotification(context, "Tidak Ada Data", notifId)
                    } else {
                        for (i in listMovie.indices) {
                            showAlarmNotification(context, listMovie[i].title, listMovie[i].id.toInt())
                        }
                    }
                }

                override fun onApiError(er: Throwable) {

                }
            })
    }

    private fun showAlarmNotification(context: Context, title: String?, notifId: Int) {

        val notificationManagerCompat = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        Log.d(TAG, "showAlarmNotification: $notifId")
        val intent = Intent(context, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(context, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder = NotificationCompat.Builder(context)
            .setSmallIcon(R.drawable.ic_movie_black_24dp)
            .setContentTitle(title)
            .setContentText("Today $title release")
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setColor(ContextCompat.getColor(context, android.R.color.transparent))
            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            .setSound(alarmSound)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            /* Create or update. */
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            builder.setChannelId(CHANNEL_ID)
            if (notificationManagerCompat != null) {
                notificationManagerCompat.createNotificationChannel(channel)
            }
        }

        notificationManagerCompat.notify(notifId, builder.build())
    }


    fun setRepeatingAlarm(context: Context) {

        cancelAlarm(context)
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val intent = Intent(context, ReleaseMovieReminder::class.java)
        val pendingIntent = PendingIntent.getBroadcast(context, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar.set(Calendar.HOUR_OF_DAY, 8)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntent
            )
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                getPendingIntent(context)
            )
        }

        Toast.makeText(context, "Repeating release set up", Toast.LENGTH_SHORT).show()

    }

    fun cancelAlarm(context: Context) {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.cancel(getPendingIntent(context))
    }

    companion object {

        private val NOTIF_ID_REPEATING = 101
        private var notifId: Int = 0

        private fun getPendingIntent(context: Context): PendingIntent {

            val intent = Intent(context, ReleaseMovieReminder::class.java)
            return PendingIntent.getBroadcast(context, 101, intent, PendingIntent.FLAG_CANCEL_CURRENT)

        }
    }
}