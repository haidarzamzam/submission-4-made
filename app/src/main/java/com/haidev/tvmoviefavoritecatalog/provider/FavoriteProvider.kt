package com.haidev.tvmoviefavoritecatalog.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.DatabaseUtils
import android.net.Uri
import com.haidev.tvmoviefavoritecatalog.db.MyDatabaseOpenHelper
import com.haidev.tvmoviefavoritecatalog.db.MyDatabaseOpenHelper.Companion.AUTHORITY
import com.haidev.tvmoviefavoritecatalog.db.MyDatabaseOpenHelper.Companion.getInstance
import com.haidev.tvmoviefavoritecatalog.db.database
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavMovieModel.Companion.TABLE_FAVORITE_MOVIE

class FavoriteProvider : ContentProvider() {

    private lateinit var databaseOpenHelper: MyDatabaseOpenHelper
    private val movieId = 1

    private val sUriMatcher = UriMatcher(UriMatcher.NO_MATCH).apply {
        this.addURI(AUTHORITY, TABLE_FAVORITE_MOVIE, movieId)
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        return null
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {

        databaseOpenHelper = getInstance(context!!)

        val uriType = sUriMatcher.match(uri)
        val cursor: Cursor
        val sql = context?.database?.readableDatabase

        cursor = when (uriType) {
            movieId -> sql?.query(
                TABLE_FAVORITE_MOVIE,
                null, null, null,
                null, null, null
            )
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }!!
        println("Dump Cursor : " + DatabaseUtils.dumpCursorToString(cursor))
        cursor.setNotificationUri(context!!.contentResolver, uri)
        return cursor
    }

    override fun onCreate(): Boolean {
        return true
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        return 0
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        return 0

    }

    override fun getType(uri: Uri): String? {
        return null
    }

}