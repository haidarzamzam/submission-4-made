package com.haidev.tvmoviefavoritecatalog.menu.favorite.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.databinding.FragmentFavoriteBinding
import com.haidev.tvmoviefavoritecatalog.menu.favorite.adapters.MyPagerAdapter
import com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels.FavoriteViewModel

class FavoriteFragment : Fragment() {

    private lateinit var favoriteBinding: FragmentFavoriteBinding
    private lateinit var vmFavorite: FavoriteViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        favoriteBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite, container, false)
        vmFavorite = ViewModelProviders.of(this).get(FavoriteViewModel::class.java)
        favoriteBinding.favorite = vmFavorite

        setupToolbar()
        favoriteBinding.viewpagerMain.adapter = MyPagerAdapter(childFragmentManager)
        favoriteBinding.tabsMain.setupWithViewPager(favoriteBinding.viewpagerMain)
        favoriteBinding.tabsMain.getTabAt(0)?.text = getString(R.string.menu_fav_movie)
        favoriteBinding.tabsMain.getTabAt(1)?.text = getString(R.string.menu_fav_tv)

        return favoriteBinding.root
    }

    private fun setupToolbar() {
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).setSupportActionBar(favoriteBinding.toolbarFav)
        }
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.txt_title_fav)
    }

}
