package com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.tvmoviefavoritecatalog.databinding.ItemListFavMovieBinding
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavMovieModel
import com.haidev.tvmoviefavoritecatalog.menu.favorite.views.DetailFavMovieActivity
import com.squareup.picasso.Picasso

class ItemFavMovieViewModel(
    private val context: Context,
    val model: FavMovieModel,
    binding: ItemListFavMovieBinding
) {

    var title: ObservableField<String?> = ObservableField(model.title)
    var desc: ObservableField<String?> = ObservableField(model.overview)
    var date: ObservableField<String?> = ObservableField(model.releaseDate)

    init {

        Picasso.get().load("https://image.tmdb.org/t/p/w185" + model.posterPath).into(binding.ivImageFavMovie)
    }

    fun clickItemListMovie(view: View) {
        val intent = Intent(context, DetailFavMovieActivity::class.java)
        intent.putExtra(DetailFavMovieActivity.EXTRA_DATA_LIST, model)
        context.startActivity(intent)
    }

}