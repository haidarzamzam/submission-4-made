package com.haidev.tvmoviefavoritecatalog.menu.movie.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.databinding.ItemListMovieBinding
import com.haidev.tvmoviefavoritecatalog.menu.movie.models.MovieModel
import com.haidev.tvmoviefavoritecatalog.menu.movie.viewmodels.ItemListMovieViewModel

class ItemMovieAdapter(private val context: Context, private var listMovie: MutableList<MovieModel.ResultsMovie>) :
    RecyclerView.Adapter<ItemMovieAdapter.ItemListMovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemListMovieViewHolder {
        val binding: ItemListMovieBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_list_movie, parent, false)
        return ItemListMovieViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listMovie.size
    }

    override fun onBindViewHolder(holder: ItemListMovieViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listMovie[fixPosition])
    }

    class ItemListMovieViewHolder(val binding: ItemListMovieBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemListMovieViewModel

        fun bindBinding(context: Context, model: MovieModel.ResultsMovie) {
            viewModel = ItemListMovieViewModel(context, model, binding)
            binding.itemMovie = viewModel
            binding.executePendingBindings()
        }

    }
}