package com.haidev.tvmoviefavoritecatalog.menu.movie.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieModel(
    @SerializedName("page") @Expose val page: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("total_results") @Expose val totalResults: Int,
    @SerializedName("results") @Expose var resultsMovie: MutableList<ResultsMovie>

) : Parcelable {

    @Parcelize
    data class ResultsMovie(
        @SerializedName("id") @Expose val id: String,
        @SerializedName("overview") @Expose val overview: String,
        @SerializedName("poster_path") @Expose val posterPath: String,
        @SerializedName("release_date") @Expose val releaseDate: String,
        @SerializedName("title") @Expose val title: String
    ) : Parcelable

}