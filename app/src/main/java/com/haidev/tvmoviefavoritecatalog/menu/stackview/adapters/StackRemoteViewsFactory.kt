package com.haidev.tvmoviefavoritecatalog.menu.stackview.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.bumptech.glide.request.target.Target
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.db.database
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavMovieModel
import com.haidev.tvmoviefavoritecatalog.menu.stackview.views.FavoriteAppWidget
import com.haidev.tvmoviefavoritecatalog.utils.glide.GlideApp
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

internal class StackRemoteViewsFactory(private val mContext: Context) : RemoteViewsService.RemoteViewsFactory {

    private var listFavMovie: MutableList<FavMovieModel> = mutableListOf()

    override fun onCreate() {

    }

    override fun getLoadingView(): RemoteViews? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0

    }

    override fun onDataSetChanged() {

        mContext.database.use {
            listFavMovie.clear()
            val result = select(FavMovieModel.TABLE_FAVORITE_MOVIE)
            val favorite = result.parseList(classParser<FavMovieModel>())
            listFavMovie.addAll(favorite)
        }

    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getViewAt(position: Int): RemoteViews? {
        val rv = RemoteViews(mContext.packageName, R.layout.widget_item)

        val posterUrl = "https://image.tmdb.org/t/p/w342" + listFavMovie[position].posterPath
        Log.d("CekPosterUrl", "posterUrl = $posterUrl")

        val bmp: Bitmap = GlideApp.with(mContext)
            .asBitmap()
            .load(posterUrl)
            .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
            .get()

        rv.setImageViewBitmap(R.id.imageView, bmp)

        val extras = Bundle()
        extras.putInt(FavoriteAppWidget.EXTRA_ITEM, position)
        val fillInIntent = Intent()
        fillInIntent.putExtras(extras)

        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent)
        return rv
    }

    override fun getCount(): Int {
        return listFavMovie.size
    }

    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun onDestroy() {

    }
}