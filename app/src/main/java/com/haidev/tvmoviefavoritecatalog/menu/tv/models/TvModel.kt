package com.haidev.tvmoviefavoritecatalog.menu.tv.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TvModel(
    @SerializedName("page") @Expose val page: Int,
    @SerializedName("total_pages") @Expose val totalPages: Int,
    @SerializedName("total_results") @Expose var totalResults: Int,
    @SerializedName("results") @Expose val resultsTv: MutableList<ResultTv>
) : Parcelable {

    @Parcelize
    data class ResultTv(
        @SerializedName("first_air_date") @Expose val firstAirDate: String,
        @SerializedName("id") @Expose val id: String,
        @SerializedName("name") @Expose val name: String,
        @SerializedName("overview") @Expose val overview: String,
        @SerializedName("poster_path") @Expose val posterPath: String
    ) : Parcelable
}