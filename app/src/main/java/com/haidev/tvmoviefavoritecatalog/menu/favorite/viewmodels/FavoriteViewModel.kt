package com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class FavoriteViewModel(application: Application) : AndroidViewModel(application)