package com.haidev.tvmoviefavoritecatalog.menu.movie.views


import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.alarm.views.SettingsActivity
import com.haidev.tvmoviefavoritecatalog.databinding.FragmentMovieBinding
import com.haidev.tvmoviefavoritecatalog.menu.movie.adapters.ItemMovieAdapter
import com.haidev.tvmoviefavoritecatalog.menu.movie.models.MovieModel
import com.haidev.tvmoviefavoritecatalog.menu.movie.viewmodels.MovieViewModel
import kotlinx.android.synthetic.main.fragment_movie.*


class MovieFragment : Fragment() {

    private lateinit var movieBinding: FragmentMovieBinding
    private lateinit var vmMovie: MovieViewModel

    private lateinit var adapter: ItemMovieAdapter
    private var listMovie: MutableList<MovieModel.ResultsMovie> = mutableListOf()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        movieBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie, container, false)
        vmMovie = ViewModelProviders.of(this).get(MovieViewModel::class.java)
        movieBinding.movie = vmMovie

        return movieBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vmMovie.listMovieResponse.observe(this, Observer {
            onListDataChange(it)
        })
        vmMovie.error.observe(this, Observer {
            handlingError(it)
        })

        setupToolbar()
        setupSwipeRefresh()
        setupRecycler()
        setHasOptionsMenu(true)
        vmMovie.requestListMovie()
    }

    private fun setupToolbar() {
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).setSupportActionBar(movieBinding.toolbarMovie)
        }
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.txt_title_movie)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.option_menu, menu)

        val searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView?
        searchView?.queryHint = getString(R.string.txt_search_movie)

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                movieBinding.progressBarMovie.isVisible = true
                vmMovie.requestSearchMovie(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            R.id.action_setting -> {
                val intent = Intent(context, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onListDataChange(movieModel: MovieModel?) {
        listMovie.clear()
        listMovie.addAll(movieModel?.resultsMovie!!)
        rvListMovie.post {
            adapter.notifyDataSetChanged()
        }
        movieBinding.progressBarMovie.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        Toast.makeText(context, throwable?.message, Toast.LENGTH_SHORT).show()
        movieBinding.progressBarMovie.isVisible = false
    }


    private fun setupSwipeRefresh() {
        movieBinding.swipeRefreshLayoutMovie.setOnRefreshListener {
            movieBinding.swipeRefreshLayoutMovie.isRefreshing = false
            vmMovie.requestListMovie()
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        movieBinding.rvListMovie.layoutManager = lManager
        movieBinding.rvListMovie.setHasFixedSize(true)

        adapter = ItemMovieAdapter(context!!, listMovie)
        movieBinding.rvListMovie.adapter = adapter
    }


}
