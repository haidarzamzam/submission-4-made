package com.haidev.tvmoviefavoritecatalog.menu.favorite.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.haidev.tvmoviefavoritecatalog.menu.favorite.views.FavMovieFragment
import com.haidev.tvmoviefavoritecatalog.menu.favorite.views.FavTvFragment

class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val pages = listOf(
        FavMovieFragment(),
        FavTvFragment()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }


}