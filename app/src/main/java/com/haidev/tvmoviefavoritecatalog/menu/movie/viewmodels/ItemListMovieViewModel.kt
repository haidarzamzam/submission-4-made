package com.haidev.tvmoviefavoritecatalog.menu.movie.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.tvmoviefavoritecatalog.databinding.ItemListMovieBinding
import com.haidev.tvmoviefavoritecatalog.menu.movie.models.MovieModel
import com.haidev.tvmoviefavoritecatalog.menu.movie.views.DetailMovieActivity
import com.squareup.picasso.Picasso

class ItemListMovieViewModel(
    private val context: Context,
    val model: MovieModel.ResultsMovie,
    binding: ItemListMovieBinding
) {

    var title: ObservableField<String?> = ObservableField(model.title)
    var desc: ObservableField<String?> = ObservableField(model.overview)
    var date: ObservableField<String?> = ObservableField(model.releaseDate)


    init {

        Picasso.get().load("https://image.tmdb.org/t/p/w185" + model.posterPath).into(binding.ivImageMovie)
    }

    fun clickItemListMovie(view: View) {
        val intent = Intent(context, DetailMovieActivity::class.java)
        intent.putExtra(DetailMovieActivity.EXTRA_DATA_LIST, model)
        context.startActivity(intent)
    }

}