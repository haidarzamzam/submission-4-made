package com.haidev.tvmoviefavoritecatalog.menu.tv.views


import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.alarm.views.SettingsActivity
import com.haidev.tvmoviefavoritecatalog.databinding.FragmentTvBinding
import com.haidev.tvmoviefavoritecatalog.menu.tv.adapters.ItemTvAdapter
import com.haidev.tvmoviefavoritecatalog.menu.tv.models.TvModel
import com.haidev.tvmoviefavoritecatalog.menu.tv.viewmodels.TvViewModel
import kotlinx.android.synthetic.main.fragment_tv.*

class TvFragment : Fragment() {

    private lateinit var tvBinding: FragmentTvBinding
    private lateinit var vmTv: TvViewModel

    private lateinit var adapter: ItemTvAdapter
    private var listTv: MutableList<TvModel.ResultTv> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        tvBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tv, container, false)
        vmTv = ViewModelProviders.of(this).get(TvViewModel::class.java)
        tvBinding.tv = vmTv

        vmTv.listTvResponse.observe(this, Observer {
            onListDataChange(it)
        })
        vmTv.error.observe(this, Observer {
            handlingError(it)
        })
        setupToolbar()
        setupSwipeRefresh()
        setupRecycler()
        setHasOptionsMenu(true)
        vmTv.requestListTv()

        return tvBinding.root
    }

    private fun setupToolbar() {
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).setSupportActionBar(tvBinding.toolbarTv)
        }
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.txt_title_tv)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.option_menu, menu)

        val searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView?
        searchView?.queryHint = getString(R.string.txt_search_tv)

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                tvBinding.progressBarTv.isVisible = true
                vmTv.requestSearchTv(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            R.id.action_setting -> {
                val intent = Intent(context, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setupSwipeRefresh() {
        tvBinding.swipeRefreshLayoutTv.setOnRefreshListener {
            tvBinding.swipeRefreshLayoutTv.isRefreshing = false
            vmTv.requestListTv()
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        tvBinding.rvListTv.layoutManager = lManager
        tvBinding.rvListTv.setHasFixedSize(true)

        adapter = ItemTvAdapter(context!!, listTv)
        tvBinding.rvListTv.adapter = adapter
    }

    private fun onListDataChange(modelTv: TvModel?) {
        listTv.clear()
        listTv.addAll(modelTv?.resultsTv!!)
        rvListTv.post {
            adapter.notifyDataSetChanged()
        }
        tvBinding.progressBarTv.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        tvBinding.progressBarTv.isVisible = false
        Toast.makeText(context, throwable?.message, Toast.LENGTH_SHORT).show()
    }

}