package com.haidev.tvmoviefavoritecatalog.menu.movie.viewmodels

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.haidev.tvmoviefavoritecatalog.menu.movie.models.MovieModel
import com.haidev.tvmoviefavoritecatalog.network.repositories.MovieRepository

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    var isLoading: ObservableField<Boolean> = ObservableField()
    var listMovieResponse: MutableLiveData<MovieModel> = MutableLiveData()
    var error: MutableLiveData<Throwable> = MutableLiveData()

    private var mainRepository = MovieRepository()

    fun requestListMovie() {
        isLoading.set(true)
        mainRepository.getListMovie({
            isLoading.set(false)
            listMovieResponse.postValue(it)
        }, {
            isLoading.set(false)
            error.postValue(it)
        })
    }

    fun requestSearchMovie(keyword: String) {
        isLoading.set(true)
        mainRepository.getSearchMovie({
            isLoading.set(false)
            listMovieResponse.postValue(it)
        }, {
            isLoading.set(false)
            error.postValue(it)
        },
            keyword
        )
    }

    override fun onCleared() {
        super.onCleared()
        mainRepository.cleared()
    }
}