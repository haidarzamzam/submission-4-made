package com.haidev.tvmoviefavoritecatalog.menu.tv.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.tvmoviefavoritecatalog.databinding.ItemListTvBinding
import com.haidev.tvmoviefavoritecatalog.menu.tv.models.TvModel
import com.haidev.tvmoviefavoritecatalog.menu.tv.views.DetailTvActivity
import com.squareup.picasso.Picasso

class ItemListTvViewModel(private val context: Context, val model: TvModel.ResultTv, binding: ItemListTvBinding) {

    var title: ObservableField<String?> = ObservableField(model.name)
    var desc: ObservableField<String?> = ObservableField(model.overview)
    var date: ObservableField<String?> = ObservableField(model.firstAirDate)


    init {

        Picasso.get().load("https://image.tmdb.org/t/p/w185" + model.posterPath).into(binding.ivImageTv)
    }

    fun clickItemListTv(view: View) {
        val intent = Intent(context, DetailTvActivity::class.java)
        intent.putExtra(DetailTvActivity.EXTRA_DATA_LIST, model)
        context.startActivity(intent)
    }

}