package com.haidev.tvmoviefavoritecatalog.menu.tv.views

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.db.database
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavTvModel
import com.haidev.tvmoviefavoritecatalog.menu.tv.models.TvModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_tv.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailTvActivity : AppCompatActivity() {

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    private lateinit var idTv: String
    private lateinit var listTvModel: TvModel.ResultTv

    companion object {
        const val EXTRA_DATA_LIST = "extra_data_list"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_tv)
        setSupportActionBar(toolbarDetailTv)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        listTvModel = intent.getParcelableExtra(EXTRA_DATA_LIST)
        idTv = listTvModel.id

        setupData()
        favTvDbState()
    }


    private fun setupData() {
        if (intent.extras != null) {
            Picasso.get().load("https://image.tmdb.org/t/p/w185" + listTvModel.posterPath).into(ivTv)
            txtTitleTv.text = listTvModel.name
            txtDateTv.text = listTvModel.firstAirDate
            txtDescTv.text = listTvModel.overview

            val judul = listTvModel.name
            supportActionBar?.title = getString(R.string.txt_detail) + " $judul"
        }
    }

    private fun favTvDbState() {
        database.use {
            val result = select(FavTvModel.TABLE_FAVORITE_TV)
                .whereArgs(
                    "(ID_FAVORITE_TV = {id})",
                    "id" to idTv
                )
            val favorite = result.parseList(classParser<FavTvModel>())
            if (favorite.isNotEmpty()) isFavorite = true
        }
    }

    private fun addToFavTvDb() {
        try {
            database.use {
                insert(
                    FavTvModel.TABLE_FAVORITE_TV,
                    FavTvModel.ID_FAVORITE_TV to listTvModel.id,
                    FavTvModel.TITLE_FAVORITE_TV to listTvModel.name,
                    FavTvModel.OVERVIEW_FAVORITE_TV to listTvModel.overview,
                    FavTvModel.POSTER_FAVORITE_TV to listTvModel.posterPath,
                    FavTvModel.DATE_FAVORITE_TV to listTvModel.firstAirDate
                )
            }
            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeFromFavTvDb() {
        try {
            database.use {
                delete(
                    FavTvModel.TABLE_FAVORITE_TV,
                    "(ID_FAVORITE_TV = {id})",
                    "id" to idTv
                )
            }
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setFavTvDb() {
        if (isFavorite)
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_white_24dp)
        else
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_white_24dp)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        menuItem = menu
        setFavTvDb()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.action_favorite) {
            if (isFavorite) removeFromFavTvDb() else addToFavTvDb()

            isFavorite = !isFavorite
            setFavTvDb()

            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
