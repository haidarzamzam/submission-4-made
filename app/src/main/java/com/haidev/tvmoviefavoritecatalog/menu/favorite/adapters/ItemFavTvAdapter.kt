package com.haidev.tvmoviefavoritecatalog.menu.favorite.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.databinding.ItemListFavTvBinding
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavTvModel
import com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels.ItemFavTvViewModel

class ItemFavTvAdapter(private val context: Context, private var listFavTv: MutableList<FavTvModel>) :
    RecyclerView.Adapter<ItemFavTvAdapter.ItemFavTvViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemFavTvViewHolder {
        val binding: ItemListFavTvBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_list_fav_tv, parent, false)
        return ItemFavTvViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listFavTv.size
    }

    override fun onBindViewHolder(holder: ItemFavTvViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listFavTv[fixPosition])
    }

    class ItemFavTvViewHolder(val binding: ItemListFavTvBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemFavTvViewModel

        fun bindBinding(context: Context, model: FavTvModel) {
            viewModel = ItemFavTvViewModel(context, model, binding)
            binding.itemFavTv = viewModel
            binding.executePendingBindings()
        }

    }
}