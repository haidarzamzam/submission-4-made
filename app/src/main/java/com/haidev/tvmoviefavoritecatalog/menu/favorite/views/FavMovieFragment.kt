package com.haidev.tvmoviefavoritecatalog.menu.favorite.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.databinding.FragmentFavMovieBinding
import com.haidev.tvmoviefavoritecatalog.db.database
import com.haidev.tvmoviefavoritecatalog.menu.favorite.adapters.ItemFavMovieAdapter
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavMovieModel
import com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels.FavMovieViewModel
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavMovieFragment : Fragment() {

    private lateinit var favMovieBinding: FragmentFavMovieBinding
    private lateinit var vmFavMovie: FavMovieViewModel

    private var isExist: Boolean = false
    private lateinit var adapter: ItemFavMovieAdapter
    private var listFavMovie: MutableList<FavMovieModel> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        favMovieBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_fav_movie, container, false)
        vmFavMovie = ViewModelProviders.of(this).get(FavMovieViewModel::class.java)
        favMovieBinding.favMovie = vmFavMovie

        favMovieDbState()
        setupRecycler()
        return favMovieBinding.root
    }


    override fun onResume() {
        super.onResume()
        favMovieDbState()
        setupRecycler()
    }

    private fun favMovieDbState() {
        context?.database?.use {
            val result = select(FavMovieModel.TABLE_FAVORITE_MOVIE)
            val datas = result.parseList(classParser<FavMovieModel>())
            if (datas.isNotEmpty()) isExist = true
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        favMovieBinding.rvFavMovie.layoutManager = lManager
        favMovieBinding.rvFavMovie.setHasFixedSize(true)

        adapter = ItemFavMovieAdapter(context!!, listFavMovie)

        favMovieBinding.rvFavMovie.adapter = adapter
        if (isExist) {
            favMovieBinding.txtNoDataMovie.visibility = View.INVISIBLE
            showFavorite()
        } else {
            favMovieBinding.txtNoDataMovie.visibility = View.VISIBLE
        }
    }

    private fun showFavorite() {
        context?.database?.use {
            listFavMovie.clear()
            val result = select(FavMovieModel.TABLE_FAVORITE_MOVIE)
            val favorite = result.parseList(classParser<FavMovieModel>())
            listFavMovie.addAll(favorite)
            adapter.notifyDataSetChanged()

        }
    }
}
