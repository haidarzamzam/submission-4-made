package com.haidev.tvmoviefavoritecatalog.menu.favorite.views

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.db.database
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavMovieModel
import com.haidev.tvmoviefavoritecatalog.menu.stackview.views.FavoriteAppWidget
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_fav_movie.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailFavMovieActivity : AppCompatActivity() {

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    private var idMovie: Long = 0
    private lateinit var listFavMovieModel: FavMovieModel

    companion object {
        const val EXTRA_DATA_LIST = "extra_data_list"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_fav_movie)
        setSupportActionBar(toolbarFavDetailMovie)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        listFavMovieModel = intent.getParcelableExtra(EXTRA_DATA_LIST)

        idMovie = listFavMovieModel.id

        setupData()
        favMovieDbState()
    }

    private fun setupData() {
        if (intent.extras != null) {
            Picasso.get().load("https://image.tmdb.org/t/p/w185" + listFavMovieModel.posterPath).into(ivFavDetaiMovie)
            txtFavTitleMovie.text = listFavMovieModel.title
            txtFavDateMovie.text = listFavMovieModel.releaseDate
            txtFavDescMovie.text = listFavMovieModel.overview

            val judul = listFavMovieModel.title
            supportActionBar?.title = getString(R.string.txt_detail) + " $judul"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.action_favorite) {
            if (isFavorite) removeFromFavMovieDb() else addToFavMovieDb()

            isFavorite = !isFavorite
            setFavMovieDb()

            val appWidgetManager = AppWidgetManager.getInstance(this)
            val thisWidget = ComponentName(this, FavoriteAppWidget::class.java)
            val appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget)
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.stack_view)

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun favMovieDbState() {
        database.use {
            val result = select(FavMovieModel.TABLE_FAVORITE_MOVIE)
                .whereArgs(
                    "(ID_FAVORITE_MOVIE = {id})",
                    "id" to idMovie
                )
            val favorite = result.parseList(classParser<FavMovieModel>())
            if (favorite.isNotEmpty()) isFavorite = true
        }
    }

    private fun addToFavMovieDb() {
        try {
            database.use {
                insert(
                    FavMovieModel.TABLE_FAVORITE_MOVIE,
                    FavMovieModel.ID_FAVORITE_MOVIE to listFavMovieModel.id,
                    FavMovieModel.TITLE_FAVORITE_MOVIE to listFavMovieModel.title,
                    FavMovieModel.OVERVIEW_FAVORITE_MOVIE to listFavMovieModel.overview,
                    FavMovieModel.POSTER_FAVORITE_MOVIE to listFavMovieModel.posterPath,
                    FavMovieModel.DATE_FAVORITE_MOVIE to listFavMovieModel.releaseDate
                )
            }
            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeFromFavMovieDb() {
        try {
            database.use {
                delete(
                    FavMovieModel.TABLE_FAVORITE_MOVIE,
                    "(ID_FAVORITE_MOVIE = {id})",
                    "id" to idMovie
                )
            }
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setFavMovieDb() {
        if (isFavorite)
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_white_24dp)
        else
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_white_24dp)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        menuItem = menu
        setFavMovieDb()
        return true
    }

}
