package com.haidev.tvmoviefavoritecatalog.menu.favorite.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.databinding.FragmentFavTvBinding
import com.haidev.tvmoviefavoritecatalog.db.database
import com.haidev.tvmoviefavoritecatalog.menu.favorite.adapters.ItemFavTvAdapter
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavTvModel
import com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels.FavTvViewModel
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavTvFragment : Fragment() {

    private lateinit var favTvBinding: FragmentFavTvBinding
    private lateinit var vmFavTv: FavTvViewModel

    private var isExist: Boolean = false
    private var listFavTv: MutableList<FavTvModel> = mutableListOf()
    private lateinit var adapter: ItemFavTvAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        favTvBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_fav_tv, container, false)
        vmFavTv = ViewModelProviders.of(this).get(FavTvViewModel::class.java)
        favTvBinding.favTv = vmFavTv

        favTvDbState()
        setupRecycler()
        return favTvBinding.root
    }

    override fun onResume() {
        super.onResume()
        favTvDbState()
        setupRecycler()
    }

    private fun favTvDbState() {
        context?.database?.use {
            val result = select(FavTvModel.TABLE_FAVORITE_TV)
            val datas = result.parseList(classParser<FavTvModel>())
            if (datas.isNotEmpty()) isExist = true
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        favTvBinding.rvFavTv.layoutManager = lManager
        favTvBinding.rvFavTv.setHasFixedSize(true)

        adapter = ItemFavTvAdapter(context!!, listFavTv)
        favTvBinding.rvFavTv.adapter = adapter
        if (isExist) {
            favTvBinding.txtNoDataTv.visibility = View.GONE
            showFavorite()
        } else {
            favTvBinding.txtNoDataTv.visibility = View.VISIBLE
        }

    }

    private fun showFavorite() {
        context?.database?.use {
            listFavTv.clear()
            val result = select(FavTvModel.TABLE_FAVORITE_TV)
            val favorite = result.parseList(classParser<FavTvModel>())
            listFavTv.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }
}
