package com.haidev.tvmoviefavoritecatalog.menu.favorite.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.databinding.ItemListFavMovieBinding
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavMovieModel
import com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels.ItemFavMovieViewModel

class ItemFavMovieAdapter(private val context: Context, private var listFavMovie: MutableList<FavMovieModel>) :
    RecyclerView.Adapter<ItemFavMovieAdapter.ItemFavMovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemFavMovieViewHolder {
        val binding: ItemListFavMovieBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_list_fav_movie, parent, false)
        return ItemFavMovieViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listFavMovie.size
    }

    override fun onBindViewHolder(holder: ItemFavMovieViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listFavMovie[fixPosition])
    }

    class ItemFavMovieViewHolder(val binding: ItemListFavMovieBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemFavMovieViewModel

        fun bindBinding(context: Context, model: FavMovieModel) {
            viewModel = ItemFavMovieViewModel(context, model, binding)
            binding.itemFavMovie = viewModel
            binding.executePendingBindings()
        }

    }
}