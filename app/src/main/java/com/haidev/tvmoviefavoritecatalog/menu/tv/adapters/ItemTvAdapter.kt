package com.haidev.tvmoviefavoritecatalog.menu.tv.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.tvmoviefavoritecatalog.R
import com.haidev.tvmoviefavoritecatalog.databinding.ItemListTvBinding
import com.haidev.tvmoviefavoritecatalog.menu.tv.models.TvModel
import com.haidev.tvmoviefavoritecatalog.menu.tv.viewmodels.ItemListTvViewModel

class ItemTvAdapter(private val context: Context, private var listTv: MutableList<TvModel.ResultTv>) :
    RecyclerView.Adapter<ItemTvAdapter.ItemListTvViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemListTvViewHolder {
        val binding: ItemListTvBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_list_tv, parent, false)
        return ItemListTvViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listTv.size
    }

    override fun onBindViewHolder(holder: ItemListTvViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listTv[fixPosition])
    }

    class ItemListTvViewHolder(val binding: ItemListTvBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemListTvViewModel

        fun bindBinding(context: Context, model: TvModel.ResultTv) {
            viewModel = ItemListTvViewModel(context, model, binding)
            binding.itemTv = viewModel
            binding.executePendingBindings()
        }

    }
}