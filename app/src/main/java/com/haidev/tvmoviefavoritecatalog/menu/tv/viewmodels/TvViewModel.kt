package com.haidev.tvmoviefavoritecatalog.menu.tv.viewmodels

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.haidev.tvmoviefavoritecatalog.menu.tv.models.TvModel
import com.haidev.tvmoviefavoritecatalog.network.repositories.TvRepository

class TvViewModel(application: Application) : AndroidViewModel(application) {
    var isLoading: ObservableField<Boolean> = ObservableField()
    var listTvResponse: MutableLiveData<TvModel> = MutableLiveData()
    var error: MutableLiveData<Throwable> = MutableLiveData()

    private var mainRepository = TvRepository()

    fun requestListTv() {
        isLoading.set(true)
        mainRepository.getListTv({
            isLoading.set(false)
            listTvResponse.postValue(it)
        }, {
            isLoading.set(false)
            error.postValue(it)
        })
    }

    fun requestSearchTv(keyword: String) {
        isLoading.set(true)
        mainRepository.getSearchTv({
            isLoading.set(false)
            listTvResponse.postValue(it)
        }, {
            isLoading.set(false)
            error.postValue(it)
        },
            keyword
        )
    }
    override fun onCleared() {
        super.onCleared()
        mainRepository.cleared()
    }
}
