package com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class FavMovieViewModel(application: Application) : AndroidViewModel(application)