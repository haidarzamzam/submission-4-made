package com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class FavTvViewModel(application: Application) : AndroidViewModel(application)