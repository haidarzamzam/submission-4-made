package com.haidev.tvmoviefavoritecatalog.menu.favorite.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class FavTvModel(
    val id: Long,
    val title: String,
    val overview: String,
    val posterPath: String,
    val releaseDate: String
) : Parcelable {

    // variable yang akan menjadi tag
    companion object {
        const val TABLE_FAVORITE_TV: String = "TABLE_FAVORITE_TV"
        const val ID_FAVORITE_TV: String = "ID_FAVORITE_TV"
        const val TITLE_FAVORITE_TV: String = "TITLE_FAVORITE_TV"
        const val OVERVIEW_FAVORITE_TV: String = "OVERVIEW_FAVORITE_TV"
        const val POSTER_FAVORITE_TV: String = "POSTER_FAVORITE_TV"
        const val DATE_FAVORITE_TV: String = "DATE_FAVORITE_TV"
    }
}
