package com.haidev.tvmoviefavoritecatalog.menu.favorite.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.tvmoviefavoritecatalog.databinding.ItemListFavTvBinding
import com.haidev.tvmoviefavoritecatalog.menu.favorite.models.FavTvModel
import com.haidev.tvmoviefavoritecatalog.menu.favorite.views.DetailFavTvActivity
import com.squareup.picasso.Picasso

class ItemFavTvViewModel(
    private val context: Context,
    val model: FavTvModel,
    binding: ItemListFavTvBinding
) {

    var title: ObservableField<String?> = ObservableField(model.title)
    var desc: ObservableField<String?> = ObservableField(model.overview)
    var date: ObservableField<String?> = ObservableField(model.releaseDate)

    init {

        Picasso.get().load("https://image.tmdb.org/t/p/w185" + model.posterPath).into(binding.ivImageFavTv)
    }

    fun clickItemListTv(view: View) {
        val intent = Intent(context, DetailFavTvActivity::class.java)
        intent.putExtra(DetailFavTvActivity.EXTRA_DATA_LIST, model)
        context.startActivity(intent)
    }
}